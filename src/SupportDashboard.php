<?php

/**
 * Defines the SiteSupportDashboard class.
 */
class SupportDashboard {

  /**
   * Stores the submitted form values.
   */
  protected $values;

  /**
   * Create an instance of this class.
   */
  public static function create($values) {
    return new static($values);
  }

  /**
   * Class constructor.
   */
  public function __construct($values) {
    $this->values = $values;
  }

  /**
   * View support information.
   *
   * @return array
   *   A renderable array.
   */
  public function view() {
    $output = [
      '#prefix' => '<div class="support-dashboard-results">',
      '#suffix' => '</div>',
    ];

    if ($hank_info = HankTools::lookup($this->values['lookup'])) {

      if (is_object($hank_info)) {
        if ($hank_info->isStudent() || $hank_info->isApplicant()) {
          $this->build($output, $hank_info);
        }
        else {
          $output[] = $this->nonStudent($hank_info);
        }
      }
      elseif (is_array($hank_info)) {
        $output['person_search'] = $hank_info;
      }
    }
    else {
      $output[] = ['#markup' => t('Person not found.')];
    }
    return $output;
  }

  /**
   * Build output for person who is not a student.
   */
  private function nonStudent($hank_info) {
    return [
      'heading' => [
        '#prefix' => '<h2>',
        '#markup' => $hank_info->formatId() . " " . $hank_info->getFullName(),
        '#suffix' => '</h2>'
      ],
      'message' => [
        '#markup' => t('This person has <strong>no student information</strong> to display.'),
      ],
      '#weight' => '-99',
    ];
  }

  /**
   * Build the output array contents.
   */
  private function build(&$output, $hank_info) {
    $hank_id = $hank_info->id();

    // Display the support summary.

    $output['myhank_support_summary'] = [
      'heading' => [
        '#prefix' => '<h2>',
        '#markup' => $hank_info->formatId() . " " . $hank_info->getFullName(),
        '#suffix' => '</h2>'
      ],
      'summary' => [
        '#theme' => 'item_list',
        '#items' => $this->getSummary($hank_info),
      ],
      '#weight' => '-99',
    ];

    // Load and alter the student profile block.

    $profile = StudentSupportBlock::create($hank_id)->content();
    $output['student_profile'] = [
      'title' => ['#prefix' => '<h2>', '#markup' => t('Student Profile'), '#suffix' => '</h2>'],
      'profile' => $profile,
      '#weight' => -20,
    ];

    // Display student schedule and textbook link, if applicable.

    if ($hank_info->isStudent() && $hank_info->getStudentSchedule()) {
      $output['student_schedule'] = [
        'title' => ['#prefix' => '<h2>', '#markup' => t('Class Schedule'), '#suffix' => '</h2>'],
        'schedule' => StudentScheduleBlock::create($hank_id)->content(),
        '#weight' => -12,
      ];
      // Cannot use l() here because the Nebraska book links are not valid URL queries.
      $output['student_textbooks'] = [
        'title' => ['#prefix' => '<h2>', '#markup' => t('Textbook Link'), '#suffix' => '</h2>'],
        'target' => ['#markup' => '<a href="' . HankTools::TEXTBOOKS . "?" . StudentBookstoreLink::generate($hank_id) .
          '" class="new-window-info" target="_blank">Textbooks</a>'
        ],
        '#weight' => -11,
      ];
    }

    // Allow other modules to add information.

    drupal_alter('myhank_support_dashboard', $output, $hank_info);

    // Alter the display weight for alerts, if any were added by drupal_alter().

    if (isset($output['mysite_alerts'])) {
      $output['mysite_alerts']['#weight'] = -19;
    }
  }

  /**
   * Build the support summary output.
   */
  private function getSummary($hank_info) {
    $summary = [];

    // Check H19_IDV_PRIVACY_FLAG information.

    if (!empty($hank_info->getPrivacyFlag())) {
      switch($hank_info->getPrivacyFlag()) {
        case 'R':
          $summary[] = t('<strong class="privacy-restriction">Privacy Restriction - Do Not Release Any Information</strong>');
          break;
        case 'D':
          $summary[] = t('<strong class="privacy-restriction">Duplicate Record - Do Not Use</strong>');
          break;
        case 'B':
          $summary[] = t('<strong class="privacy-restriction">Student is Banned from Campus - Contact Campus Safety - 9630</strong>');
          break;
      }
    }

    // Check Identity Info refresh flag.

    if (($update_flag = $hank_info->getIdentityAttribute('H19_IDVC_FLAG')) && ($update_flag == 'Y')) {
      $summary[] = t('<strong>Update Pending:</strong> Please wait up to 15 minutes for HANK changes to update Identity Management info.');
    }

    // Display basic troubleshooting info.

    $cell_phone = !empty($hank_info->getIdentityAttribute('H19_IDV_CELL_PHONE')) ? $hank_info->getIdentityAttribute('H19_IDV_CELL_PHONE') : 'none';
    $summary[] = t('<strong>Password Reset Cell Phone:</strong> @cell', ['@cell' => $cell_phone]);

    $network_role = $hank_info->getNetworkRole();
    $summary[] = t('<strong>Network Role:</strong> @role', ['@role' => $network_role]);

    if (in_array($network_role, [NULL, 'APPLICANT', 'ADMITTED'])) {
      $expire_date = 'N/A (no network login)';
      $expire_class = 'loginsearch-neutral';
    }
    elseif ($expires = $hank_info->getIdentityAttribute('H19_IDV_EXPIRE_DATE_UX')) {
      $expire_date = format_date($expires, 'custom', 'F j, Y');
      $expire_class = (REQUEST_TIME > $expires) ? 'loginsearch-expired' : 'loginsearch-not-expired';
    }
    else {
      $expire_date = 'none';
      $expire_class = 'loginsearch-neutral';
    }

    $summary[] = [
      'data' => t('<strong>Network Login Expiration Date:</strong> @date', ['@date' => $expire_date]),
      'class' => [$expire_class],
    ];

    return $summary;
  }
}
