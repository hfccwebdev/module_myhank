<?php

/**
 * Defines the HankPersonContainerInterface.
 */
interface HankPersonContainerInterface {

  /**
   * Retrieves the static container.
   *
   * @return \StdClass
   *   The active global container.
   */
  public static function getContainer();
}

class HankPersonContainer implements HankPersonContainerInterface {

  /**
   * Stores the Person info.
   *
   * @var \StdClass
   */
  protected static $container;

  /**
   * {@inheritdoc}
   */
  public static function getContainer() {
    if (static::$container === NULL) {
      static::$container = new StdClass();
    }
    return static::$container;
  }
}
