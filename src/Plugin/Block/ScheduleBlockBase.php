<?php

/**
 * Defines the ScheduleBlockBase.
 *
 * Because Drupal 7's stupid autoloader does not support Traits.
 */
class ScheduleBlockBase extends HankBlockBase {

  /**
   * Stores the current academic term.
   */
  protected $current_term;

    /**
   * Build content for this block.
   */
  protected function build(&$output) {
    parent::build($output);

    $this->current_term = WebServicesClient::getCurrentTerm();
  }

  /**
   * Formats a class schedule list.
   */
  protected function formatClassList($schedule) {

    if ($schedule['#start_date'] > REQUEST_TIME) {
      $divclasses = ['schedule-future'];
      $values = ['@date' => format_date($schedule['#start_date'], 'custom', 'l, F j')];
      if ($schedule['#term'] == $this->current_term) {
        $message = 'The following classes start on @date.';
      }
      else {
        $divclasses[] = 'schedule-' . preg_replace('|/|', '', drupal_strtolower($schedule['#term']));
        $values['@term'] = WebServicesClient::getTermName($schedule['#term']);
        $message = 'The following @term classes start on @date.';
      }
      $output = $this->scheduleWrapper($divclasses);
      $output[] = [
        '#prefix' => '<div class="future-start-date-notice">',
        '#markup' => t($message, $values),
        '#suffix' => '</div>',
      ];
    }
    else {
      $output = $this->scheduleWrapper(['schedule-current']);
    }

    $labels = [t('Sunday'), t('Monday'), t('Tuesday'), t('Wednesday'), t('Thursday'), t('Friday'), t('Saturday')];
    for ($i=0; $i < 7; $i++) {
      if (!empty($schedule[$i])) {
        $output[] = ['#prefix' => '<h3>', '#markup' => $labels[$i], '#suffix' => '</h3>'];
        $keys = array_keys($schedule[$i]);
        sort($keys);
        $items = [];
        foreach ($keys as $key) {
          $row =  '<span class="class-time">' . date("g:i a", strtotime($key));
          $row .= ' - ' . date("g:i a", strtotime($schedule[$i][$key]['end_time'])) . '</span>';
          $row .= '<span class="class-name">' . $schedule[$i][$key]['course_name'] . '</span>';
          $row .= !empty($schedule[$i][$key]['room']) ? '<span class="class-room">' . $schedule[$i][$key]['room'] . '</span>' : "";
          $items[] = $row;
        }
        $output[] = ['#theme' => 'item_list', '#items' => $items];
      }
    }
    if (!empty($schedule[8])) {
      $items = [];
      $output[] = ['#prefix' => '<h3>', '#markup' => t('Other'), '#suffix' => '</h3>'];
      foreach ($schedule[8] as $section) {
        $items[] = '<span class="class-name">' . $section['course_name'] . '</span><span="class-method">(' . $section['instr_method'] . ')</span>';
      }
      $output[] = ['#theme' => 'item_list', '#items' => $items];
    }
    return $output;
  }

  /**
   * Generate wrapper div for schedule list.
   */
  protected function scheduleWrapper($classes) {
    return [
      '#prefix' => '<div class="' . implode(' ', $classes) . '">',
      '#suffix' => '</div>',
    ];
  }
}
