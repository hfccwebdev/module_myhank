<?php

/**
 * Provides the student profile block.
 */
class StudentProfileBlock extends HankProfileBlock {

  /**
   * Returns values for hook_block_info().
   */
  public function info() {
    return [
      'info' => t('Student Profile'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * Returns the block label.
   */
  public function label() {
    return t('My Student Profile');
  }

  /**
   * Build content for this block.
   */
  protected function build(&$output) {
    parent::build($output);

    if ($this->isStudent()) {

      $output['profile_info']['profile_info_fields']['hank_id']['#label'] = t('Student ID');

      if ($program = $this->getStudentActiveProgram()) {

        $output['profile_info']['profile_info_fields']['student_program'] = [
          '#field_name' => 'myhank_student_program',
          '#label' => t('Program'),
          '#label_display' => 'inline',
          '#markup' => l($program, self::MYPROGRESS, ['attributes' => ['target' => '_blank', 'class' => 'new-window-info']]),
          '#theme' => 'hfcc_global_pseudo_field',
          '#weight' => -13,
        ];
      }
      if ($advisors = $this->getStudentAdvisorContacts()) {
        $output['profile_info']['profile_info_fields']['student_advisors'] = [
          '#field_name' => 'myhank_student_advisors',
          '#label' => t('@label', ['@label' => format_plural(count($advisors), 'Advisor', 'Advisors')]),
          '#label_display' => 'inline',
          '#markup' => implode(', ', $advisors),
          '#theme' => 'hfcc_global_pseudo_field',
          '#weight' => -12,
        ];
      }
      if ($gpa = $this->getStudentGPA()) {
        $output['profile_info']['profile_info_fields']['student_gpa'] = [
          '#field_name' => 'myhank_student_gpa',
          '#label' => t('GPA'),
          '#label_display' => 'inline',
          '#markup' => number_format($gpa, 2),
          '#theme' => 'hfcc_global_pseudo_field',
          '#weight' => -11,
        ];
      }
      $output['profile_info']['profile_info_fields']['student_balance'] = [
        '#field_name' => 'myhank_sss_finance_link',
        '#label' => t('Account Summary'),
        '#label_display' => 'hidden',
        '#markup' => l('View account balance', self::MYBALANCE, ['attributes' => ['target' => '_blank', 'class' => 'new-window-info']]),
        '#theme' => 'hfcc_global_pseudo_field',
        '#weight' => -1,
      ];
    }
  }

}
