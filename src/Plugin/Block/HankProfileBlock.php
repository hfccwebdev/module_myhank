<?php

/**
 * Defines the HANK Profile Block.
 */
class HankProfileBlock extends HankBlockBase {

  /**
   * Returns values for hook_block_info().
   */
  public function info() {
    return [
      'info' => t('My HANK Profile'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * Returns the block label.
   */
  public function label() {
    return t('My Profile');
  }

  /**
   * Build content for this block.
   */
  protected function build(&$output) {
    parent::build($output);

    $output['profile_header'] = [
      '#prefix' => '<div class="myhank-profile-header clearfix border-separator">',
      '#suffix' => '</div>',
    ];

    $output['profile_header']['welcome_profile'] = [
      '#prefix' => '<div class="welcome-profile">',
      '#suffix' => '</div>',
      '#weight' => -15,
    ];

    $output['profile_header']['profile_header_fields'] = [
      '#prefix' => '<div class="myhank-profile-header-fields">',
      '#suffix' => '</div>',
      '#weight' => -14,
    ];

    $output['profile_header']['welcome_profile']['full_name'] = [
      '#field_name' => 'myhank_full_name',
      '#label' => t('Full Name'),
      '#label_display' => 'hidden',
      '#markup' => t('Welcome !name', ['!name' => $this->getFullName()]),
      '#theme' => 'hfcc_global_pseudo_field',
      '#weight' => -20,
    ];

    $output['profile_header']['info_toggle'] = [
      '#markup' => t('<button id="button-profile-info-toggle"><span class="profile-button-text">Show Profile Information</span><span class="view-profile-information-button expand-plus"></span></button>'),
      '#weight' => 50,
    ];

    $output['profile_info'] = [
      '#prefix' => '<div class="myhank-profile-info clearfix hide-profile-info">',
      '#suffix' => '</div>',
    ];

    if ($id_photo = $this->getIdPhoto()) {
      $output['profile_info']['photo'] = [
        '#prefix' => '<div class="myhank-idphoto">',
        '#markup' => $id_photo,
        '#suffix' => '</div>',
        '#weight' => -15,
      ];
    }

    $output['profile_info']['profile_info_fields'] = [
      '#prefix' => '<div class="profile-info-fields">',
      '#suffix' => '</div>',
    ];

    $output['profile_info']['profile_info_fields']['hank_id'] = [
      '#field_name' => 'myhank_hankid',
      '#label' => t('HANK ID'),
      '#label_display' => 'inline',
      '#markup' => $this->person->hank_id,
      '#theme' => 'hfcc_global_pseudo_field',
      '#weight' => -14,
    ];

    if ($phones = $this->getPhoneNumbers()) {
      $output['profile_info']['profile_info_fields']['phone_numbers'] = [
        '#field_name' => 'myhank_phones',
        '#label' => t('Phone Number'),
        '#label_display' => count($phones) > 1 ? 'above' : 'inline',
        '#items' => $phones,
        '#theme' => 'hfcc_global_pseudo_field',
        '#weight' => 18,
      ];
    }

    if ($email = $this->getEmailAddress()) {
      $output['profile_info']['profile_info_fields']['email_address'] = [
        '#field_name' => 'myhank_email',
        '#label' => t('Email Address'),
        '#label_display' => 'inline',
        '#markup' => $email,
        '#theme' => 'hfcc_global_pseudo_field',
        '#weight' => 19,
      ];
    }

    $output['profile_info']['profile_info_fields']['sss_profile_link'] = [
      '#field_name' => 'myhank_sss_profile_link',
      '#label' => t('Update HANK Profile'),
      '#label_display' => 'hidden',
      '#markup' => l('Update Profile Information', self::MYPROFILE, ['attributes' => ['target' => '_blank', 'class' => 'new-window-info']]),
      '#theme' => 'hfcc_global_pseudo_field',
      '#weight' => 20,
    ];

    $output['region-dashboard-l']['profile-separator'] = [
      '#prefix' => '<div class="clearfix profile-separator">',
      '#suffix' => '</div>',
      '#weight' => 51,
    ];
  }
}
