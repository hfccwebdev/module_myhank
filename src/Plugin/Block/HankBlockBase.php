<?php

/**
 * Defines the HankBlockBaseInterface.
 */
interface HankBlockBaseInterface {

  /**
   * Returns the block label.
   *
   * @return string
   *   Returns the block label, if any.
   */
  public function label();

  /**
   * Returns form for hook_block_configure().
   *
   * @return array
   *   Returns a Form API array.
   */
  public function configure();

  /**
   * Saves configuration for hook_block_save().
   *
   * @param array $edit
   *   Configuration form submitted values.
   */
  public function save($edit);

  /**
   * Returns value for hook_block_view().
   *
   * @return array
   *   Returns a block array with subject and content.
   */
  public function view();

  /**
   * Return only the block content.
   *
   * @return array
   *   Returns a renderable array.
   */
  public function content();
}

/**
 * Defines the base block class for My HANK blocks.
 */
class HankBlockBase extends HankTools implements HankBlockBaseInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function configure() {
    $form = [];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save($edit) {
  }

  /**
   * {@inheritdoc}
   */
  public function view() {
    $output = $this->content();
    if (!empty($output)) {
      return ['subject' => $this->label(), 'content' => $output];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function content() {
    if (!empty($this->person->hank_id)) {
      $output = [];
      $this->build($output);
      return $output;
    }
  }

  /**
   * Build content for this block.
   */
  protected function build(&$output) {
  }
}
