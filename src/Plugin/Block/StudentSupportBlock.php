<?php

/**
 * Provides the student profile block.
 */
class StudentSupportBlock extends StudentProfileBlock {

  /**
   * Colleage Self-Service administrative links.
   */
  const SSS_STUDENT_FINANCE_LINK = 'https://sss.hfcc.edu/Student/Finance/AccountSummary/Admin/';
  const SSS_FINAID_LINK          = 'https://sss.hfcc.edu/Student/FinancialAid/Home/Admin/';
  const SSS_ADVISING_LINK        = 'https://sss.hfcc.edu/Student/Planning/Advisors/Advise/';

  /**
   * Returns values for hook_block_info().
   */
  public function info() {
    // This class is used to modify output from StudentProfileBlock::build() for the Support Dashboard.
    // This block should **never** be added to hook_block_info(), etc, in the module file!
    die('Do not call this block directly');
  }

  /**
   * Returns the block label.
   */
  public function label() {
    return t('Student Profile');
  }

  /**
   * Build content for this block.
   */
  protected function build(&$output) {
    parent::build($output);

    if (!empty($output)) {
      $output['profile_info']['#prefix'] = '<div class="myhank-profile-info clearfix">';
      unset($output['profile_header']['info_toggle']);
      unset($output['profile_info']['profile_info_fields']['student_gpa']);
      unset($output['profile_info']['profile_info_fields']['sss_profile_link']);

      $options = ['attributes' => ['target' => '_blank', 'class' => 'new-window-info']];

      // Rewrite administrative link for Student Advising.

      if (!empty($output['profile_info']['profile_info_fields']['student_program'])) {
        $program = $this->getStudentActiveProgram();
        $path = self::SSS_ADVISING_LINK . $this->formatId();
        $output['profile_info']['profile_info_fields']['student_program']['#markup'] = l($program, $path, $options);
      }

      // Rewrite administrative link for Student Finance.

      $path = self::SSS_STUDENT_FINANCE_LINK . $this->formatId();
      $output['profile_info']['profile_info_fields']['student_balance']['#markup'] = l('View account balance', $path, $options);

      // Add administrative link for Financial Aid.

      $path = self::SSS_FINAID_LINK . $this->formatId();
      $output['profile_info']['profile_info_fields']['financial_aid'] = [
        '#field_name' => 'myhank_sss_financial_aid_link',
        '#label' => t('Financial Aid'),
        '#label_display' => 'hidden',
        '#markup' => l('View financial aid info', $path, $options),
        '#theme' => 'hfcc_global_pseudo_field',
        '#weight' => 0,
      ];
    }
  }
}
