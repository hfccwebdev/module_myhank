<?php

/**
 * Provides the student schedule block.
 */
class StudentScheduleBlock extends ScheduleBlockBase {

  /**
   * Returns values for hook_block_info().
   */
  public static function info() {
    return [
      'info' => t('Student Schedule'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * Returns the block label.
   */
  public function label() {
    return t('My Class Schedule');
  }

  /**
   * Build content for this block.
   */
  protected function build(&$output) {
    parent::build($output);

    $schedule = [];

    if ($classes = $this->getStudentSchedule()) {
      foreach($classes as $class) {
        foreach ($class->MEETINGINFO as $meeting) {
          if (empty($schedule[$meeting->CSM_START_DATE_UX]) && (REQUEST_TIME <= $meeting->CSM_END_DATE_UX)) {
            $schedule[$meeting->CSM_START_DATE_UX] = [
              '#start_date' => $meeting->CSM_START_DATE_UX,
              '#term' => $class->STC_TERM,
            ];
          }
          if ($meeting->CSM_MEETING_DAYS == '_______' && (REQUEST_TIME <= $meeting->CSM_END_DATE_UX)) {
            $schedule[$meeting->CSM_START_DATE_UX][8][] = [
              'course_name' => $class->STC_COURSE_NAME,
              'instr_method' => $this->instrMethods($meeting->CSM_INSTR_METHOD),
            ];
          }
          else {
            for ($i=0; $i < 7; $i++) {
              if (
                  drupal_substr($meeting->CSM_MEETING_DAYS, $i, 1) !== "_" &&
                  !empty($meeting->CSM_START_TIME) &&
                  REQUEST_TIME <= $meeting->CSM_END_DATE_UX
              ) {
                $csm_start_time = date("H:i", strtotime($meeting->CSM_START_TIME));
                $csm_end_time = date("H:i", strtotime($meeting->CSM_END_TIME));
                $schedule[$meeting->CSM_START_DATE_UX][$i][$csm_start_time] = [
                  'course_name' => $class->STC_COURSE_NAME,
                  'start_date' => $meeting->CSM_START_DATE_UX,
                  'end_date' => $meeting->CSM_END_DATE_UX,
                  'start_time' => $csm_start_time,
                  'end_time' => $csm_end_time,
                  'room' => $meeting->CSM_ROOM,
                ];
              }
            }
          }
        }
      }
    }

    if (!empty($schedule)) {
      ksort($schedule);
      $output['schedule'] = [
        '#prefix' => '<div class="schedule-detail">',
        '#suffix' => '</div>',
      ];

      if (user_access('access schedule page')) {
        $output['schedule']['printable'] = ['#markup' => t('<div class="print-schedule"><a href="/students/schedule">Print my Schedule</a></div>')];
      }

      foreach ($schedule as $starting_date => $classes) {
        $output['schedule']["schedule-$starting_date"] = $this->formatClassList($classes);
      }

    }
    elseif ($this->isStudent()) {
      $output[] = [
        '#markup' => t(
          'You have no scheduled classes. <a href="@profile" class="new-window-info" target="_blank">Go to Student Planning</a>.',
          ['@profile' => self::MYPLAN]
        ),
      ];
    }
    else {
      $output[] = ['#markup' => t('No student information.')];
    }
  }
}
