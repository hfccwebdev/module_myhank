<?php

/**
 * Defines the Employee Profile block.
 */
class EmployeeProfileBlock extends HankProfileBlock {

  /**
   * Returns values for hook_block_info().
   */
  public function info() {
    return [
      'info' => t('Employee Profile'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * Returns the block label.
   */
  public function label() {
    return t('My Profile');
  }

  /**
   * Build block content.
   */
  protected function build (&$output) {
    parent::build($output);

    if ($hank_employee = $this->getEmployeeInfo()) {

      $output['profile_info']['profile_info_fields']['hank_id']['#label'] = t('Employee ID');

      // Position Title
      if (!empty($hank_employee->POS_TITLE)) {
        $output['profile_info']['profile_info_fields']['position_title'] = [
          '#field_name' => 'myhank_position_title',
          '#label' => t('Position Title'),
          '#label_display' => 'hidden',
          '#markup' => $hank_employee->POS_TITLE,
          '#theme' => 'hfcc_global_pseudo_field',
          '#weight' => -19,
        ];
      }

      // Office Location
      if ($office_location = $this->getOfficeLocation()) {
        $output['profile_info']['profile_info_fields']['office_location'] = [
          '#field_name' => 'myhank_office_location',
          '#label' => t('Office Location'),
          '#label_display' => 'inline',
          '#markup' => $office_location,
          '#theme' => 'hfcc_global_pseudo_field',
          '#weight' => 2,
        ];
      }

      // Office Phone
      if ($office_phone = $this->getOfficePhone(TRUE)) {
        $output['profile_info']['profile_info_fields']['office_phone'] = [
          '#field_name' => 'myhank_office_phone',
          '#label' => t('Office Phone'),
          '#label_display' => 'inline',
          '#markup' => $office_phone,
          '#theme' => 'hfcc_global_pseudo_field',
          '#weight' => 3,
        ];
      }
    }
  }
}
