<?php

/**
 * @file
 * Defines the HANK Tools Interface and Service.
 *
 * @ingroup hfcc_modules
 */

/**
 * Defines the HANK Tools Interface.
 */
interface HankToolsInterface {

  /**
   * Instantiates a new object of this class.
   *
   * @param string $hank_id
   *   The HANK ID of the PERSON.
   *
   * @return $this
   *   Returns an initialized object.
   */
  public static function create($hank_id = NULL);

  /**
   * Retrieve the HANK ID of the current Drupal user.
   *
   * @return string
   *   Returns a string containing the user's HANK ID.
   */
  public static function myHankId();

  /**
   * Look up a person.
   *
   * @param string $search_id
   *   HANK ID, Username, or Surname to query.
   *
   * @return HankToolsInterface|array|null
   *   Returned results
   *   - If single match returns an initialized object.
   *   - If surname match, returns an array of matches.
   *   - If no match, returns NULL.
   *
   * @see GetPersonInfo::build()
   */
  public static function lookup($search_id);

  /**
   * Return the HANK ID of the currently-loaded person.
   *
   * @return int
   *   Returns the integer value of the person's HANK ID.
   */
  public function id();

  /**
   * Return the zero-padded HANK ID of the currently-loaded person.
   *
   * @return string
   *   Returns the string value of the person's HANK ID.
   */
  public function formatId();

  /**
   * Return the encrypted HANK ID of the currently-loaded person.
   *
   * @return string
   *   Returns the string value of the person's encrypted HANK ID.
   */
  public function eid();

  /**
   * Return the network username.
   *
   * @return string
   *   The person's network username.
   */
  public function username();

  /**
   * Get the HANK Identity privacy flag.
   *
   * @return string
   *   The person's privacy flag.
   *   - R: Restricted - do not release information.
   *   - D: Duplicate Record - do not use.
   *   - B: Banned from campus. Contact Campus Safety at 9911.
   */
  public function getPrivacyFlag();

  /**
   * Retrieve the Portal Constituency of the current user.
   *
   * @return string
   *   The person's primary portal constituency.
   */
  public function getConstituency();

  /**
   * Retrieve the Network Role of the current user.
   *
   * @return string
   *   The person's network role.
   */
  public function getNetworkRole();

  /**
   * Retrieve the specified hank_identity attribute, if set.
   *
   * @param string $property
   *   A valid property name.
   *
   * @return string
   *   The requested property.
   */
  public function getIdentityAttribute($property);

  /**
   * Get the person's full name from hank_identity data.
   *
   * Uses Drupal values if available.
   *
   * @return string
   *   The person's full name.
   */
  public function getFullName();

  /**
   * Get the person's known phone numbers.
   *
   * @return string[]
   *   An array of telephone numbers.
   */
  public function getPhoneNumbers();

  /**
   * Get the person's HFC-assigned email address.
   *
   * @return string
   *   The person's email address, formatted as an HTML mailto: link.
   */
  public function getEmailAddress();

  /**
   * Get student checklist status.
   *
   * @return StdClass[]
   *   Checklist flags in Y/N format.
   */
  public function getStudentChecklist();

  /**
   * Get student class schedule.
   *
   * @return StdClass[]
   *   An array of schedule objects.
   */
  public function getStudentSchedule(): array;

  /**
   * Get student GPA.
   *
   * @return string
   *   The student's Grade Point Average.
   */
  public function getStudentGPA();

  /**
   * Get student total credits.
   *
   * @return StdClass
   *   The student's total cumulative and total transfer credits.
   */
  public function getStudentCompletedCredits();

  /**
   * Get student Active Program.
   *
   * @return string
   *   The student's currently active academic program.
   */
  public function getStudentActiveProgram();

  /**
   * Get student's assigned advisor(s).
   *
   * @return string[]
   *   An array of contact information for the student's advisor(s).
   */
  public function getStudentAdvisorContacts();

  /**
   * Get student priority registration info.
   *
   * @return StdClass[]
   *   An array of priority registration dates, by term.
   */
  public function getPriorityRegistration();

  /**
   * Get faculty class schedule.
   *
   * @return StdClass[]
   *   An array of schedule objects.
   */
  public function getFacultySchedule();

  /**
   * Get employee position information.
   *
   * @return StdClass
   *   The employee position data.
   */
  public function getEmployeeInfo();

  /**
   * Get an employee's campus office location.
   *
   * @return string
   *   The person's formatted office location.
   */
  public function getOfficeLocation();

  /**
   * Get an employee's formatted office phone number.
   *
   * @param bool $allow_extension
   *   Allow displaying 4-digit internal extensions.
   *
   * @return string
   *   The person's office phone number.
   */
  public function getOfficePhone($allow_extension = FALSE);

  /**
   * Get ID photo.
   *
   * @return string
   *   Base64-encoded phot data.
   */
  public function getIdPhoto();

  /**
   * Determine if the person is an applicant.
   *
   * @return bool
   *   TRUE if the person has the network role APPLICANT.
   *
   * @see https://dvc.hfcc.net/hank/issues/issue5268
   */
  public function isApplicant();

  /**
   * Determine if the person is a student.
   *
   * @return bool
   *   TRUE if the person is a member of ALLSTUDENTS.
   */
  public function isStudent();

  /**
   * Determine if the person is an employee.
   *
   * @return bool
   *   TRUE if the person is a member of ALLEMPLOYEES.
   */
  public function isEmployee();

  /**
   * Determine if the person is faculty.
   *
   * @return bool
   *   TRUE if the person is a member of ALLFACULTY.
   */
  public function isFaculty();

  /**
   * Determine if a student has ever taken classes.
   *
   * @return bool
   *   TRUE if student has a schedule or cumulative (non-transfer) credits.
   */
  public function hasTakenClasses();

  /**
   * Get student academic level.
   *
   * @return string
   *   Student Academic Level
   *   - UG: Undergrad
   *   - WF: Workforce Development
   *   - CE: Continuing Education
   */
  public function getStudentAcadLevel();

  /**
   * Determine if student is undergrad level.
   *
   * @return bool
   *   TRUE if student academic level is UG.
   */
  public function isUndergrad();

  /**
   * Look up instructional method description.
   *
   * @return string[]
   *   An array of instrutional method descriptions, keyed by valcode.
   */
  public function instrMethods($value);

  /**
   * Clear all cached HANK API data for the current person.
   */
  public function cacheClear();

  /**
   * Generate a refresh link to clear caches.
   *
   * @return string
   *   A formatted HTML link.
   */
  public function refreshLink();

  /**
   * Format an integer as academic year.
   *
   * @param int|string $year
   *   The year to display.
   *
   * @return string
   *   A formatted Academic Year
   */
  public function formatAcYr($year);

  /**
   * Show person info.
   *
   * @return string
   *   Raw dump of the person's hank_identity and hank_person data.
   *   For diagnostics purposes only.
   */
  public function showInfo();

}

/**
 * Defines the HANK Tools Service.
 *
 * @ingroup hfcc_modules
 */
class HankTools implements HankToolsInterface {

  /**
   * Program progress.
   */
  const MYPROGRESS = 'https://sss.hfcc.edu/Student/Planning/Programs/MyProgress';

  /**
   * Account Balance.
   */
  const MYBALANCE = 'https://sss.hfcc.edu/Student/Finance';

  /**
   * User profile.
   */
  const MYPROFILE = 'https://sss.hfcc.edu/Student/UserProfile';

  /**
   * Student Planning.
   */
  const MYPLAN = 'https://sss.hfcc.edu/Student/Planning';

  /**
   * College store textbook link.
   */
  const TEXTBOOKS = 'https://hfcc.verbacompare.com/textbook_express/_get_txtexpress.asp';

  /**
   * SSRS class rosters report.
   */
  const SSRS_ROSTER = 'faculty-and-staff/report-viewer/roster';

  /**
   * SSRS attendance report.
   */
  const SSRS_ATTENDANCE = 'faculty-and-staff/report-viewer/attendance';

  /**
   * New and improved static person container.
   *
   * @var HankPersonContainer
   */
  protected $person;

  /**
   * HANK API Cache Prefix.
   *
   * @var string
   */
  protected $cachePrefix;

  /**
   * Construct new object.
   */
  public function __construct($hank_id = NULL) {
    $this->checkId($hank_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function create($hank_id = NULL) {
    return new static($hank_id);
  }

  /**
   * Retrieves the current user's HANK ID if one was not provided.
   */
  protected function checkId(&$hank_id = NULL) {

    // Get the container object and set its properties on first-time load.
    $this->person = HankPersonContainer::getContainer();

    if (isset($this->person->initialized)) {
      return;
    }

    if (empty($hank_id)) {
      $this->person->hank_id = $this->myHankId();
      global $user;
      $this->person->user = $user;
    }
    else {
      $this->person->hank_id = $hank_id;
      $this->person->user = $this->getUserFromHankId();
    }

    if (!empty($this->person->hank_id)) {
      $this->cachePrefix = "HankTools_{$this->person->hank_id}";
      $cid = HfcHankApi::getCacheID('timestamp', $this->cachePrefix);
      if ($result = cache_get($cid)) {
        if (REQUEST_TIME - $result->data > 300) {
          $this->cacheClear();
          cache_set($cid, REQUEST_TIME);
        }
      }
      else {
        cache_set($cid, REQUEST_TIME);
      }
      $this->getIdentity();
    }

    $this->person->initialized = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function myHankId() {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      global $user;
      if ($user->uid > 0) {
        $user = user_load($user->uid);
        if (!empty($user->field_user_hank_id[LANGUAGE_NONE][0]['value'])) {
          $output = $user->field_user_hank_id[LANGUAGE_NONE][0]['value'];
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function lookup($search_id) {

    // Sanitize input.
    $id = check_plain($search_id);
    if (is_numeric($id)) {
      $id = str_pad($id, 7, "0", STR_PAD_LEFT);
    }
    else {
      $id = drupal_strtolower($id);
    }

    // Search for matches.
    if ($hank_identity = HfcHankApi::getData('hank-identity-info', $id, NULL, FALSE)) {
      $hank_identity = reset($hank_identity);
      return HankTools::create($hank_identity->H19_IDV_ID);
    }
    elseif ($hank_search = HfcHankApi::getData('getpersoninfo-lastname', $id, NULL, FALSE)) {
      return [
        '#label' => t('HANK Person Lookup'),
        '#theme' => 'myhank_person_search',
        '#data' => $hank_search,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return (int) $this->person->hank_id;
  }

  /**
   * {@inheritdoc}
   */
  public function formatId() {
    return $this->person->hank_id;
  }

  /**
   * {@inheritdoc}
   */
  public function eid() {
    return !empty($this->person->hank_identity->EID) ? $this->person->hank_identity->EID : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function username() {
    return $this->person->hank_identity->H19_IDV_OEE_USERNAME;
  }

  /**
   * Get HANK Identity Information.
   */
  protected function getIdentity() {
    $hank_identity = HfcHankApi::getData('hank-identity-info', $this->person->hank_id, NULL, TRUE, $this->cachePrefix);
    if (!empty($hank_identity) && ($first = $this->firstChild($hank_identity))) {
      $this->person->hank_identity = $first;
      $hank_person = HfcHankApi::getData('getpersoninfo-person', $this->person->hank_id, NULL, TRUE, $this->cachePrefix);
      if (!empty($hank_person) && ($first = $this->firstChild($hank_person))) {
        $this->person->hank_person = $first;
      }
      else {
        $this->person->hank_person = new StdClass();
      }
    }
    else {
      $this->person->hank_identity = new StdClass();
      $this->person->hank_person = new StdClass();
    }
  }

  /**
   * Find the user from the current HANK ID.
   */
  private function getUserFromHankId() {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'user')
      ->fieldCondition('field_user_hank_id', 'value', $this->person->hank_id);
    if ($result = $query->execute()) {
      $user = reset($result['user']);
      return user_load($user->uid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPrivacyFlag() {
    return $this->getIdentityAttribute('H19_IDV_PRIVACY_FLAG');
  }

  /**
   * {@inheritdoc}
   */
  public function getConstituency() {
    return $this->getIdentityAttribute('H19_IDV_PRIMARY_CONST');
  }

  /**
   * {@inheritdoc}
   */
  public function getNetworkRole() {
    return $this->getIdentityAttribute('H19_IDV_ROLE');
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentityAttribute($property) {
    if (!empty($this->person->hank_identity->$property)) {
      return $this->person->hank_identity->$property;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFullName() {
    if (!empty($this->person->full_name)) {
      return $this->person->full_name;
    }
    if (!empty($this->person->hank_identity->H19_IDV_PREFERRED_NAME)) {
      $fullname = $this->person->hank_identity->H19_IDV_PREFERRED_NAME;
    }
    elseif (!empty($this->person->hank_identity->H19_IDV_FIRST_NAME) || !empty($this->person->hank_identity->H19_IDV_LAST_NAME)) {
      $names = [];
      foreach ([
        'H19_IDV_FIRST_NAME',
        'H19_IDV_MIDDLE_INIT',
        'H19_IDV_LAST_NAME',
        'H19_IDV_SUFFIX',
      ] as $fieldname) {
        if (!empty($this->person->hank_identity->$fieldname)) {
          $names[] = $this->person->hank_identity->$fieldname;
        }
      }
      $fullname = implode(' ', $names);
    }
    elseif (!empty($this->person->user->field_user_full_name)) {
      $fullname = $this->person->user->field_user_full_name[LANGUAGE_NONE][0]['safe_value'];
    }
    elseif (!empty($this->person->user->field_user_first_name) || !empty($this->person->user->field_user_last_name)) {
      $names = [];
      foreach (['field_user_first_name', 'field_user_last_name'] as $fieldname) {
        if (!empty($this->person->user->field_user_first_name)) {
          $names[] = $this->person->user->{$fieldname}[LANGUAGE_NONE][0]['safe_value'];
        }
      }
      $fullname = implode(' ', $names);
    }
    else {
      $fullname = $this->person->user->name;
    }
    $this->person->full_name = $fullname;
    return $fullname;
  }

  /**
   * {@inheritdoc}
   */
  public function getPhoneNumbers() {

    if (!empty($this->person->hank_person->PHONE)) {
      $phone_types = [
        'B' => 'Business Only',
        'C' => 'Cell',
        'CO' => 'Campus Office',
        'F' => 'Fax',
        'H' => 'Home',
        'W' => 'Work',
      ];
      $phones = [];
      foreach ($this->person->hank_person->PHONE as $phone) {
        $phones[] = t('@type: @number', [
          '@type' => !empty($phone_types[$phone->PERSONAL_PHONE_TYPE])
            ? $phone_types[$phone->PERSONAL_PHONE_TYPE]
            : $phone->PERSONAL_PHONE_TYPE,
          '@number' => $phone->PERSONAL_PHONE_NUMBER,
        ]);
      }
      return $phones;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailAddress() {
    if (!empty($this->person->hank_person->EMAIL)) {
      foreach ($this->person->hank_person->EMAIL as $email) {
        if ($email->PERSON_EMAIL_TYPES == 'H') {
          return l($email->PERSON_EMAIL_ADDRESSES, 'mailto:' . $email->PERSON_EMAIL_ADDRESSES);
        }
      }
    }
    elseif (!empty($this->person->user->mail)) {
      return l($this->person->user->mail, 'mailto:' . $this->person->user->mail);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStudentSchedule(): array {
    if (!$this->isStudent()) {
      return [];
    }

    if (!isset($this->person->student_schedule)) {
      $this->person->student_schedule = HfcHankApi::getData('student-schedule', $this->person->hank_id, NULL, TRUE, $this->cachePrefix);
    }
    return $this->person->student_schedule ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getStudentGPA() {
    if (isset($this->person->gpa)) {
      return $this->person->gpa;
    }
    if ($this->isStudent() && ($result = HfcHankApi::getData('student-termcreds', $this->person->hank_id, NULL, TRUE, $this->cachePrefix))) {
      foreach ($result as $term) {
        if ($term->TERM == 'CUMU') {
          $this->person->gpa = $term->CUM_GPA_AS_OF_TERM;
          return $this->person->gpa;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStudentCompletedCredits() {
    if (isset($this->person->completed_credits)) {
      return $this->person->completed_credits;
    }
    if ($this->isStudent() && ($result = HfcHankApi::getData('student-termcreds', $this->person->hank_id, NULL, TRUE, $this->cachePrefix))) {
      $output = (object) ['cumu' => 0, 'tran' => 0, 'gpa' => 0];
      foreach ($result as $term) {
        if ($term->TERM == 'CUMU') {
          $output->cumu = $term->TERM_CUM_CMPL_CREDITS;
          $output->gpa = $term->CUM_GPA_AS_OF_TERM;
        }
        elseif ($term->TERM == 'TRAN') {
          $output->tran = $term->TERM_CUM_CMPL_CREDITS;
        }
      }
      $this->person->completed_credits = $output;
      return $output;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStudentActiveProgram() {
    if (isset($this->person->current_program)) {
      return $this->person->current_program;
    }
    if ($this->isStudent() && ($result = HfcHankApi::getData('student-acadprogram', $this->person->hank_id, NULL, TRUE, $this->cachePrefix))) {
      if ($acadprogram = $this->firstChild($result)) {
        $this->person->current_program = !empty($acadprogram->CURRENT_ACAD_PROGRAM) ? $acadprogram->CURRENT_ACAD_PROGRAM : NULL;
        return $this->person->current_program;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStudentAdvisorContacts() {
    if (isset($this->person->student_advisors)) {
      return $this->person->student_advisors;
    }
    if ($this->isStudent()) {
      if ($advisors = HfcHankApi::getData('student-advisors', $this->person->hank_id, NULL, TRUE, $this->cachePrefix)) {
        array_map(function ($advisor) {
          $name = $advisor->ADVISOR_FIRST_NAME . " " . $advisor->ADVISOR_LAST_NAME;
          $this->person->student_advisors[$advisor->STAD_FACULTY] = l($name, "mailto:{$advisor->ADVISOR_EMAIL}");
        }, $advisors);
        return $this->person->student_advisors;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPriorityRegistration() {
    if ($this->isStudent()) {
      return HfcHankApi::getData('student-regpriorities', $this->person->hank_id, NULL, TRUE, $this->cachePrefix);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStudentChecklist() {
    if (isset($this->person->student_checklist)) {
      return is_object($this->person->student_checklist)
        ? $this->person->student_checklist
        : NULL;
    }
    if ($this->isApplicant() || $this->isUndergrad()) {
      $result = HfcHankApi::getData('student-checklist', $this->person->hank_id, NULL, TRUE, $this->cachePrefix);
      if (is_array($result)) {
        $this->person->student_checklist = reset($result);
        return $this->person->student_checklist;
      }
      else {
        $this->person->student_checklist = 'ERROR';
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFacultySchedule() {
    if ($this->isFaculty()) {
      if (!isset($this->person->faculty_schedule)) {
        $this->person->faculty_schedule = HfcHankApi::getData('faculty-schedule', $this->person->hank_id, NULL, TRUE, $this->cachePrefix);
      }
      return $this->person->faculty_schedule;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEmployeeInfo() {
    if ($this->isEmployee()) {
      if (!isset($this->person->employee_info)) {
        $hank_employee = HfcHankApi::getData('getpersoninfo-employee', $this->person->hank_id, NULL, TRUE, $this->cachePrefix);
        $this->person->employee_info = is_array($hank_employee) ? reset($hank_employee) : $hank_employee;
      }
      return $this->person->employee_info;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOfficeLocation() {
    if (!empty($this->person->hank_identity->H19_IDV_CAMPUS_OFFICE)) {
      $office_location = $this->person->hank_identity->H19_IDV_CAMPUS_OFFICE;
    }
    elseif (!empty($this->person->hank_identity->H19_IDV_CAMPUS_BUILDING)) {
      $office_location = "Building {$this->person->hank_identity->H19_IDV_CAMPUS_BUILDING}";
    }
    else {
      $office_location = t('Not listed in HANK.');
    }
    return $office_location;
  }

  /**
   * {@inheritdoc}
   */
  public function getOfficePhone($allow_extension = FALSE) {
    if (isset($this->person->hank_identity->H19_IDV_CAMPUS_PHONE)) {
      return HfcGlobalUtils::formatOfficePhone($this->person->hank_identity->H19_IDV_CAMPUS_PHONE, $allow_extension);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getIdPhoto() {
    if (!empty($this->person->id_photo)) {
      return $this->person->id_photo;
    }
    elseif ($photo = HfcHankApi::getPhoto($this->person->hank_id, TRUE, $this->cachePrefix)) {
      $this->person->id_photo = $photo;
      return $photo;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicant() {
    if (!empty($this->person->hank_identity->H19_IDV_ROLE)) {
      return $this->person->hank_identity->H19_IDV_ROLE == 'APPLICANT';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isStudent() {
    if (!empty($this->person->hank_identity->H19_IDV_GROUPS)) {
      return in_array("ALLSTUDENTS", explode(',', $this->person->hank_identity->H19_IDV_GROUPS));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isEmployee() {
    if (!empty($this->person->hank_identity->H19_IDV_GROUPS)) {
      return in_array("ALLEMPLOYEES", explode(',', $this->person->hank_identity->H19_IDV_GROUPS));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isFaculty() {
    if (!empty($this->person->hank_identity->H19_IDV_GROUPS)) {
      return in_array("ALLFACULTY", explode(',', $this->person->hank_identity->H19_IDV_GROUPS));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasTakenClasses() {
    if ($credits = $this->getStudentCompletedCredits()) {
      if ($credits->cumu > $credits->tran) {
        return TRUE;
      }
    }
    return $this->getStudentSchedule() !== [];
  }

  /**
   * {@inheritdoc}
   */
  public function getStudentAcadLevel() {
    if ($this->isApplicant() || $this->isStudent()) {
      if (!empty($this->person->student_acad_level)) {
        return $this->person->student_acad_level;
      }
      elseif ($result = HfcHankApi::getData('student-acadprogram', $this->person->hank_id, NULL, TRUE, $this->cachePrefix)) {
        if ($acadprogram = $this->firstChild($result)) {
          $this->person->student_acad_level = !empty($acadprogram->STU_ACAD_LEVELS) ? $acadprogram->STU_ACAD_LEVELS : NULL;
          return $this->person->student_acad_level;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isUndergrad() {
    return $this->getStudentAcadLevel() == 'UG';
  }

  /**
   * {@inheritdoc}
   */
  public function instrMethods($value) {
    $methods = &drupal_static(__FUNCTION__);
    if (!isset($methods)) {
      $result = HfcHankApi::getData('instr-methods');
      $methods = [];
      foreach (element_children($result) as $key) {
        $method = $result[$key];
        $methods[$method->INSTR_METHODS_ID] = $method->INM_DESC;
      }
    }
    $output = !empty($methods[$value]) ? $methods[$value] : check_plain($value);
    return $output;
  }

  /**
   * Returns the first child element of an array.
   *
   * @see element_children()
   */
  protected function firstChild($element) {
    if (is_array($element)) {
      $children = element_children($element);
      $first = reset($children);
      return !is_null($first) ? $element[$first] : NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function cacheClear() {
    cache_clear_all($this->cachePrefix, 'cache', TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function refreshLink($message = NULL) {
    $options = ['attributes' => ['class' => ['refresh-link']]];
    if ($destination = drupal_get_destination()) {
      $options['query'] = $destination;
    }
    return l((empty($message) ? t('Refresh') : $message), 'hanktools/reset', $options);
  }

  /**
   * {@inheritdoc}
   */
  public function formatAcYr($year) {
    $year = is_int($year) ? $year : intval($year);
    if ($year > 1977) {
      return strval($year) . "-" . strval($year + 1);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function showInfo() {
    return print_r($this->person, 1);
  }

}
