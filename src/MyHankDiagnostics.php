<?php

/**
 * Defines the MyHankDiagnostics class.
 */
class MyHankDiagnostics {

  /**
   * Implements hook_mysite_user_diagnotics_alter().
   */
  public static function alter(&$output, &$account) {
    if (!empty($account->field_user_hank_id)) {
      $hank_id = $account->field_user_hank_id[LANGUAGE_NONE][0]['safe_value'];
      $hank_info = HankTools::create($hank_id);

      if ($hank_info->isStudent()) {
        $output['mysite_hank_data'] = [
          'student_profile' => [
            'title' => ['#prefix' => '<h2>', '#markup' => t('Student Profile'), '#suffix' => '</h2>'],
            'profile' => StudentProfileBlock::create($hank_id)->content(),
            '#weight' => -20,
          ],
          'student_schedule' => [
            'title' => ['#prefix' => '<h2>', '#markup' => t('Class Schedule'), '#suffix' => '</h2>'],
            'schedule' => StudentScheduleBlock::create($hank_id)->content(),
            '#weight' => -19,
          ],
          'student_textbooks' => [
            'title' => ['#prefix' => '<h2>', '#markup' => t('Textbook Link'), '#suffix' => '</h2>'],
            'target' => ['#markup' => check_plain(StudentBookstoreLink::generate($hank_id))],
            '#weight' => -18,
          ],
        ];
      }
      elseif ($hank_info->isEmployee()) {
        $output['mysite_hank_data'] = [
          'employee_profile' => [
            'title' => ['#prefix' => '<h2>', '#markup' => t('Employee Profile'), '#suffix' => '</h2>'],
            'profile' => EmployeeProfileBlock::create($hank_id)->content(),
            '#weight' => -15,
          ],
        ];
      }
      else {
        $output['mysite_hank_data'] = [
          'student_profile' => [
            'title' => ['#prefix' => '<h2>', '#markup' => t('Student Profile'), '#suffix' => '</h2>'],
            'profile' => StudentProfileBlock::create($hank_id)->content(),
            '#weight' => -20,
          ],
        ];
      }
      if ($hank_info->isFaculty()) {
        $output['mysite_hank_data']['faculty_schedule'] = [
          'title' => ['#prefix' => '<h2>', '#markup' => t('Faculty Class Schedule'), '#suffix' => '</h2>'],
          'profile' => FacultyScheduleBlock::create($hank_id)->content(),
          '#weight' => -14,
        ];
      }
    }
  }
}
