<?php

/**
 * Page callbacks to clear HANK API cached data.
 */
class HankToolsReset {

  /**
   * Page callback to clear caches and redirect.
   */
    public static function view($hank_id = NULL, $destination = 'user') {
    $hank_id = !empty($hank_id) ? $hank_id : HankTools::myHankId();

    $output = [];

    if ($hankinfo = HankTools::create($hank_id)) {
      $hankinfo->cacheClear();
      drupal_goto($destination);
      drupal_exit();
    }
    else {
      return t('Could not clear data.');
    }
  }

  /**
   * Page callback for specified Drupal account.
   *
   * @param StdClass $account
   *   The User Account
   *
   * @return array
   *   A renderable array.
   */
  public static function userpage($account) {
    if (!empty($account->field_user_hank_id)) {
      $hank_id = $account->field_user_hank_id[LANGUAGE_NONE][0]['safe_value'];
      return self::view($hank_id, "user/{$account->uid}/diagnostics");
    }
  }
}
