<?php

/**
 * Generate a printable schedule for faculty.
 */
class FacultySchedulePrint {

  /**
   * Page callback for schedule.
   */
  public static function view($hank_id = NULL) {
    $hank_id = !empty($hank_id) ? $hank_id : HankTools::myHankId();

    $output = [];

    if ($hankinfo = HankTools::create($hank_id)) {
      if ($hankinfo->isFaculty()) {

        $output[] = [
          '#prefix' => '<div class="faculty-name">',
          '#markup' => t('!id !name', ['!id' => $hank_id, '!name' => $hankinfo->getFullName()]),
          '#suffix' => '</div>',
        ];

        $status = [
          'A' => 'Active',
          'P' => '<strong><em>Pending</em></strong>',
        ];

        $rows = [];

        if ($classes = $hankinfo->getFacultySchedule()) {

          if (count($classes) > 50) {
            drupal_set_message(t('You are scheduled to teach %count classes. Limiting results to first 50 entries.', ['%count' => count($classes)]), 'warning');
            $classes = array_slice($classes, 0, 50);
          }

          foreach($classes as $class) {

            if ($class->SEC_END_DATE_UX >= REQUEST_TIME) {
              $meeting_info = [];
              foreach ($class->MEETINGINFO as $meeting) {
                $info = t('!start_date - !end_date<br>!method', [
                  '!start_date' => $meeting->CSM_START_DATE,
                  '!end_date' => $meeting->CSM_END_DATE,
                  '!method' => $hankinfo->instrMethods($meeting->CSM_INSTR_METHOD),
                ]);

                if ($meeting->CSM_MEETING_DAYS !== '_______') {
                  $days = self::parseWeekdays($meeting->CSM_MEETING_DAYS);
                  if (!empty($meeting->CSM_START_TIME) && !empty($meeting->CSM_END_TIME)) {
                    $days .= ' ' . drupal_strtolower(str_replace(' ', '', $meeting->CSM_START_TIME));
                    $days .= ' - ' . drupal_strtolower(str_replace(' ', '', $meeting->CSM_END_TIME));
                  }
                  $info .= "<br>$days";
                }

                // if (!preg_match('/TB(A|D)$/', $meeting->CSM_ROOM)) {
                  $location = [];
                  if (!empty($meeting->CSM_BLDG)) {
                    $location[] = WebServicesClient::getBuildingName($meeting->CSM_BLDG);
                  }
                  if (!empty($meeting->CSM_ROOM)) {
                    $location[] = 'Room ' . $meeting->CSM_ROOM;
                  }
                  if (!empty($location)) {
                    $info .= "<br>" . implode(', ', $location);
                  }
                // }

                $meeting_info[] = $info;
            }

            list($subject, $crsnum, $secnum) = explode('-', $class->SEC_NAME);

            $options = [
              'sec_term=' . $class->SEC_TERM,
              'crs_name=' . $subject . '-' . $crsnum,
              'sec_no='   . $secnum,
            ];

            if ($section = WebServicesClient::getCourseSections($options)) {
              $section = reset($section);
              $seats = t('@r / @a / @w', [
                '@r' => $section['sec_seats_taken'],
                '@a' => $section['sec_capacity'] - $section['sec_seats_taken'],
                '@w' => $section['sec_waitlist_count'],
              ]);
            }
            else {
              drupal_set_message(t('Could not retrieve seat availability information for %sec', ['%sec' => $class->SEC_NAME]), 'warning');
              $seats = 'unavailable';
            }

            $course_name = [
              check_plain(trim($class->SEC_NAME)),
              check_plain(trim($class->SEC_SHORT_TITLE)),
              (!empty($status[$class->SEC_STATUS]) ? $status[$class->SEC_STATUS] : "Unknown status <strong>{$class->SEC_STATUS}</strong>"),
            ];

            $rows[$class->SEC_TERM][] = [
              implode('<br>', $course_name),
              implode('<br><br>', $meeting_info),
              $seats,
            ];
            }
          }
        }
        if (!empty($rows)) {
          foreach ($rows as $semester => $schedule) {

            $output[] = [
              '#prefix' => '<div class="schedule-semester">',
              '#markup' => WebServicesClient::getTermName($semester),
              '#suffix' => '</div>',
            ];

            $output[] = [
              '#theme' => 'table',
              '#header' => [t('Course Name'), t('Meeting Information'), t('Reg/Avail/Wait')],
              '#rows' => $schedule,
              '#attributes' => ['summary' => [t('This is your schedule.')]],
              '#empty' =>t('You have no scheduled classes.'),
            ];
          }
          $output[] = ['#markup' => t('
            <strong>Please note:</strong> Section availability information is updated nightly.
            For current information, please visit
            <a href="https://sss.hfcc.edu/">HFC Self Service</a>.
          ')];
        }
        else {
          $output[] = [
            '#prefix' => '<p>',
            '#markup' => t('You have no scheduled classes to display.'),
            '#suffix' => '</p>',
          ];
        }
        if (user_access('clear myhank cache')) {
          $output[] = [
            '#prefix' => '<p>',
            '#markup' => $hankinfo->refreshLink('Refresh HANK data'),
            '#suffix' => '</p>',
          ];
        }
      }
      else {
        $output[] = ['#markup' => t('No faculty information.')];
      }
    }
    return $output;
  }

  /**
   * Parse weekday values.
   *
   * @param string $days
   *   Class meeting days.
   *
   * @return string
   *   Class meeting days expanded to words.
   */
  private static function parseWeekdays($days) {
    $weekdays = [
      'X' => 'Sun',
      'M' => 'Mon',
      'T' => 'Tue',
      'W' => 'Wed',
      'R' => 'Thu',
      'F' => 'Fri',
      'S' => 'Sat',
    ];

    $output = [];
    foreach(str_split($days) as $day) {
      if (!empty($weekdays[$day])) {
        $output[] = $weekdays[$day];
      }
    }
    return implode(', ', $output);
  }

  /**
   * Page callback for user's schedule.
   *
   * @param StdClass $account
   *   The User Account
   *
   * @return array
   *   A renderable array.
   */
  public static function userpage($account) {
    if (!empty($account->field_user_hank_id)) {
      $hank_id = $account->field_user_hank_id[LANGUAGE_NONE][0]['safe_value'];
      return self::view($hank_id);
    }
  }
}
