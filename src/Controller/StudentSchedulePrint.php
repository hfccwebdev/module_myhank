<?php

/**
 * Generate a printable schedule for students.
 */
class StudentSchedulePrint {

  /**
   * Page callback for schedule.
   */
  public static function view($hank_id = NULL) {
    $hank_id = !empty($hank_id) ? $hank_id : HankTools::myHankId();

    $output = [];

    if ($hankinfo = HankTools::create($hank_id)) {
      if ($hankinfo->isStudent()) {

        $output[] = [
          '#prefix' => '<div class="student-name">',
          '#markup' => t('!id !name', ['!id' => $hank_id, '!name' => $hankinfo->getFullName()]),
          '#suffix' => '</div>',
        ];

        $rows = [];

        if ($classes = $hankinfo->getStudentSchedule()) {
          foreach($classes as $class) {
            if ($class->TERM_END_DATE_UX >= REQUEST_TIME) {

              $meeting_info = [];

              foreach ($class->MEETINGINFO as $meeting) {

                $info = t('!start_date - !end_date<br>!method', [
                  '!start_date' => $meeting->CSM_START_DATE,
                  '!end_date' => $meeting->CSM_END_DATE,
                  '!method' => $hankinfo->instrMethods($meeting->CSM_INSTR_METHOD),
                ]);

                if ($meeting->CSM_MEETING_DAYS !== '_______') {
                  $days = self::parseWeekdays($meeting->CSM_MEETING_DAYS);
                  if (!empty($meeting->CSM_START_TIME) && !empty($meeting->CSM_END_TIME)) {
                    $days .= ' ' . drupal_strtolower(str_replace(' ', '', $meeting->CSM_START_TIME));
                    $days .= ' - ' . drupal_strtolower(str_replace(' ', '', $meeting->CSM_END_TIME));
                  }
                  $info .= "<br>$days";
                }

                if (!preg_match('/TB(A|D)$/', $meeting->CSM_ROOM)) {
                  $location = [];
                  if (!empty($meeting->CSM_BLDG)) {
                    $location[] = WebServicesClient::getBuildingName($meeting->CSM_BLDG);
                  }
                  if (!empty($meeting->CSM_ROOM)) {
                    $location[] = 'Room ' . $meeting->CSM_ROOM;
                  }
                  if (!empty($location)) {
                    $info .= "<br>" . implode(', ', $location);
                  }
                }

                $meeting_info[] = $info;
              }

              $options = [
                'sec_term=' . $class->STC_TERM,
                'crs_name=' . $class->STC_COURSE_NAME,
                'sec_no='   . $class->STC_SECTION_NO,
              ];

              if ($section = WebServicesClient::getCourseSections($options)) {
                $section = reset($section);
                $credits = $section['crs_min_cred'];

                if (!empty($section['faculty_last_name'])) {
                  $faculty = $section['faculty_last_name'];
                  if (!empty($section['faculty_first_name'])) {
                    $faculty .= ', ' . drupal_substr($section['faculty_first_name'], 0, 1);
                  }
                  if (!empty($section['faculty_email'])) {
                    $faculty .= '<br>' . l($section['faculty_email'], 'mailto:' . $section['faculty_email']);
                  }
                }
                else {
                  $faculty = 'TBA';
                }
              }
              else {
                $credits = 'unavailable';
                $faculty = 'unavailable';
              }

              $rows[$class->STC_TERM][] = [
                $class->STC_COURSE_NAME . '-' . $class->STC_SECTION_NO . '<br>' . $class->STC_TITLE,
                implode('<br><br>', $meeting_info),
                $credits,
                $faculty,
              ];
            }
          }

          foreach ($rows as $semester => $schedule) {

            $output[] = [
              '#prefix' => '<div class="schedule-semester">',
              '#markup' => WebServicesClient::getTermName($semester),
              '#suffix' => '</div>',
            ];

            $output[] = [
              '#theme' => 'table',
              '#header' => [t('Course Name'), t('Meeting Information'), t('Credit Hours'), t('Instructor')],
              '#rows' => $schedule,
              '#attributes' => ['summary' => [t('This is your schedule.')]],
              '#empty' =>t('You have no scheduled classes.'),
            ];
          }
        }
        if (!empty($rows)) {
          $output[] = [
            '#prefix' => '<div class="textbook-link">',
            '#markup' => l(t('My textbooks'), 'students/textbooks', ['absolute' => TRUE]),
            '#suffix' => '</div>',
          ];
        }
        else {
          $output[] = [
            '#markup' => t(
              'You have no scheduled classes. <a href="@profile" target="_blank">Go to Student Planning</a>.',
              ['@profile' => $hankinfo::MYPLAN]
            ),
          ];
        }
        if (user_access('clear myhank cache')) {
          $output[] = [
            '#prefix' => '<p>',
            '#markup' => $hankinfo->refreshLink('Refresh schedule data'),
            '#suffix' => '</p>',
          ];
        }
      }
      else {
        $output[] = ['#markup' => t('No student information.')];
      }
    }
    else {
      $output[] = ['#markup' => t('Could not retrieve HANK info. Please try again later.')];
    }
    return $output;
  }

  /**
   * Parse weekday values.
   *
   * @param string $days
   *   Class meeting days.
   *
   * @return string
   *   Class meeting days expanded to words.
   */
  private static function parseWeekdays($days) {
    $weekdays = [
      'X' => 'Sun',
      'M' => 'Mon',
      'T' => 'Tue',
      'W' => 'Wed',
      'R' => 'Thu',
      'F' => 'Fri',
      'S' => 'Sat',
    ];

    $output = [];
    foreach(str_split($days) as $day) {
      if (!empty($weekdays[$day])) {
        $output[] = $weekdays[$day];
      }
    }
    return implode(', ', $output);
  }

  /**
   * Page callback for user's schedule.
   *
   * @param StdClass $account
   *   The User Account
   *
   * @return array
   *   A renderable array.
   */
  public static function userpage($account) {
    if (!empty($account->field_user_hank_id)) {
      $hank_id = $account->field_user_hank_id[LANGUAGE_NONE][0]['safe_value'];
      return self::view($hank_id);
    }
  }
}
