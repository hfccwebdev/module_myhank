<?php

/**
 * Defines the Student Bookstore Link.
 */
class StudentBookstoreLink {

  /**
   * Generate the link arguments.
   */
  public static function generate($hank_id) {

    $terms = WebServicesClient::getTermsOpts();

    if ($classes = HankTools::create($hank_id)->getStudentSchedule()) {
      $books = [];
      foreach($classes as $class) {
        if ($class->TERM_END_DATE_UX > REQUEST_TIME) {
          list($dept, $course) = explode('-', $class->STC_COURSE_NAME);
          $books[] = implode('&', [
            "term=" . str_replace(' ', '+', $terms[$class->STC_TERM]),
            "dept=" . $dept,
            "course=" . $course,
            "section=" . $class->STC_SECTION_NO,
          ]);
        }
      }
      if (!empty($books)) {
        $args = "remote=1&ref=2032&student=$hank_id&" . implode('&', $books) . "&getbooks=display+books";
        return $args;
      }
    }
  }

  /**
   * Page callback.
   *
   * On success, this page will redirect to the College Store website.
   */
  public static function view($hank_id = NULL) {
    $hank_id = !empty($hank_id) ? $hank_id : HankTools::myHankId();
    if ($args = self::generate($hank_id)) {
      $target = HankTools::TEXTBOOKS . "?" . $args;
      drupal_goto($target);
    }
    else {
      drupal_set_title(t('My Textbooks'));
      return ['#markup' => t("Sorry, no current or upcoming class schedule found for bookstore textbook list.")];
    }
  }
}
