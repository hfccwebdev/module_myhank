<?php

/**
 * @file
 * Contains theme_myhank_person_search function for the getpersoninfo module.
 */

/**
 * Theme preprocess function for theme_myhank_person_search().
 *
 * @see template_preprocess_field()
 * @see template_preprocess_hfcc_global_pseudo_field()
 */
function template_preprocess_myhank_person_search(&$variables, $hook) {

  $element = $variables['element'];
  $items = $element['#data'];

  // Provide a default value for #label_display.

  if (empty($element['#label_display'])) {
    $element['#label_display'] = 'above';
  }
  $variables['label_hidden'] = ($element['#label_display'] == 'hidden');
  $variables['label'] = ($variables['label_hidden'] || empty($element['#label'])) ? NULL : check_plain($element['#label']);

  $content = [];

  $header = [t('HANK ID'), t('Name'), t('Birth Date') , t('Gender')];
  $attributes = ['width' => '100%', 'border' => '1', 'cellpadding' => '1', 'cellspacing' => '0'];
  $rows = [];

  foreach ($items as $item) {
    $rows[] = [
      check_plain($item->ID),
      check_plain($item->LAST_NAME . ', ' . $item->FIRST_NAME . ' ' . $item->MIDDLE_NAME),
      check_plain($item->BIRTH_DATE),
      check_plain($item->GENDER),
    ];
  }

  $content[] = [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => $attributes,
  ];

  $variables['content'] = $content;
}

/**
 * Theme function for myhank_person_search.
 *
 * @todo move this to a .tpl.php file.
 */
function theme_myhank_person_search(&$variables) {
  $output = NULL;

  if (!empty($variables['label'])) {
    $output .= '<h2>' . $variables['label'] . '</h2>';
  }

  if (!empty($variables['content'])) {
    $output .= render($variables['content']);
  }

  return $output;
}
