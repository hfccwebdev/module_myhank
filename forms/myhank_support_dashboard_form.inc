<?php

/**
 * @file
 * Provides the Student Support Dashboard
 *
 * @see mysite.module
 */

/**
 * The support dashboard form.
 */
function myhank_support_dashboard_form($form, &$form_state) {
  drupal_set_title(t('Student success dashboard'));

  if (!empty($form_state['values']['lookup'])) {
    $lookup = $form_state['values']['lookup'];
  }
  else {
    $lookup = NULL;
  }

  $form['lookup'] = [
    '#type' => 'textfield',
    '#title' => t('Login Name or HANK ID'),
    '#size' => 25,
    '#required' => TRUE,
    '#default_value' => $lookup,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Search'),
  ];

  if (!empty($form_state['values']['lookup'])) {
    $form['results'] = [
      'results' => SupportDashboard::create($form_state['values'])->view(),
      '#weight' => 99,
    ];
  }


  $form['#attributes'] = ['class' => 'support-dashboard'];

  return $form;
}

/**
 * Form submit handler.
 */
function myhank_support_dashboard_form_submit($form, &$form_state) {
  // Simply set rebuild flag and return to the form.
  $form_state['rebuild'] = TRUE;
}
